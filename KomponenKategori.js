export const KomponenKategori = {
  data() {
    return {
      kategoriKategori: [
        {
          id: 1,
          nama: 'Alat Tulis',
          deskripsi: 'Alat tulis adalah alat yang digunakan untuk menulis, di antaranya pulpen, pensil, dan penghapus.',
        },
        {
          id: 2,
          nama: 'Alat Mandi',
          deskripsi: 'Alat mandi adalah alat yang digunakan untuk mandi, di antaranya gayung, sikat gigi, dan handuk.',
        },
        {
          id: 3,
          nama: 'Alat Masak',
          deskripsi: 'Alat masak adalah alat yang digunakan untuk memasak, di antaranya panci, kompor, dan ketel.',
        },
      ],
    }
  },
  computed: {
    kategori() {
      return this.kategoriKategori.filter(kategori => {
        return kategori.id === parseInt(this.$route.params.id)
      })[0]
    },
  },
  template: `
    <div>
      <h1>Kategori: {{ kategori.nama }}</h1>
      <ul>
        <li v-for="(nilai, kunci) of kategori" :key="kunci">
          {{ kunci + ': ' + nilai }}<br>
        </li>
      </ul>
    </div>
  `,
}
