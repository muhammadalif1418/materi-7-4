export const KomponenKategoriKategori = {
  data() {
    return {
      kategoriKategori: [
        {
          id: 1,
          nama: 'Alat Tulis',
        },
        {
          id: 2,
          nama: 'Alat Mandi',
        },
        {
          id: 3,
          nama: 'Alat Masak',
        },
      ],
    }
  },
  template: `
    <div>
      <h1>Daftar Kategori</h1>
      <ul>
        <li v-for="kategori of kategoriKategori" :key="kategori.id">
          <router-link :to="'/kategori/' + kategori.id">
            {{ kategori.nama }}
          </router-link>
        </li>
      </ul>
    </div>
  `,
}
